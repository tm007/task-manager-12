package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task remove(Task task);

    Task add(Task task);

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    void clear();

}
