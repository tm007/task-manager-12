package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project task);

    List<Project> findAll();

    Project findOneByIndex(Integer index);

    Project findById(String id);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

    void clear();

}
