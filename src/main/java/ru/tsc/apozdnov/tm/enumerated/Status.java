package ru.tsc.apozdnov.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started."),

    IN_PROGRESS("In progress"),

    COMPLETED("Completed");

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public static Status toStatus(String val) {
        if (val == null || val.isEmpty()) return null;
        for (Status status : values()) {
            if (status.name().equals(val)) return status;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

}
